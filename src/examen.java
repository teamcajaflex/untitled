
import java.util.Scanner;

/**
 * Feu un programa que demana un string i filtra que tingui,
 * com a mínim,
 * longitud 8, contingui una majúscula i un número.
 * Si no compleix els tres requisits, tornarà a demanar l’String.
 *
 * @author Montse
 * @version 22/1/2020
 */

public class examen {

    public static void main(String[] args) {
        String paraula;

        boolean longitud8;
        boolean majuscula;
        boolean numero;
        Scanner entrada = new Scanner(System.in);
        do {
            longitud8 = false;
            majuscula = false;
            numero = false;

            System.out.println("Paraula?");
            paraula = entrada.next();
            //mirar si la seva longitud >= 8
            longitud8 = paraula.length() >= 8;
            if (!longitud8) System.out.println("Longitud <8");

            //mirar si conté alguna lletra majúscula (entre 'A' i 'Z')
            for (int i = 0; i < paraula.length() && !majuscula; i++) {
                if (paraula.charAt(i) >= 'A' && paraula.charAt(i) <= 'Z') majuscula = true;
            }
            if (!majuscula) System.out.println("No conté majúscula");

            //mirar si conté un número
            for (int i = 0; i < paraula.length() && !numero; i++) {
                if (paraula.charAt(i) >= '0' && paraula.charAt(i) <= '9') {
                    numero = true;
                }
            }
            if (!numero) System.out.println("Falta número");

        } while (!longitud8 || !majuscula || !numero);
        System.out.println("Tot ok");
    }
}